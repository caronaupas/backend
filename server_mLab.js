require('dotenv').config();
const express = require('express'); //import
const body_parser = require('body-parser');
const request_json = require('request-json');
const app = express();
const port = process.env.PORT || 3000;
const usersFile = require('./user.json');
const URL_BASE = '/techu/v2/';
const URL_mLab = 'https://api.mLab.com/api/1/databases/techu2db/collections/';
const apikey_mlab = 'apiKey=' + process.env.API_KEY_MLAB;

app.listen(port, function(){
  console.log('NodeJS escuchando en el puerto ' + port);
});

app.use(body_parser.json());

//Operacion GET con mLab (Collection)
 app.get(URL_BASE + 'users1',
    function(request, response){
//      const http_client = request_json.createClient(URL_mLab + '?' + apikey_mlab);
      const http_client = request_json.createClient(URL_mLab);
      console.log('Cliente HTTP a mLab creado correctamente');
      let field_params = 'f={"_id":0}&';
      http_client.get('user?' + field_params + apikey_mlab,
        function(err, res, body){
          response.send(body);
        });
  //    response.status(200).send({'msg':'Cliente HTTP a mLab creado correctamente'});
});

//Operacion GET con mLab (Collection) v2
app.get(URL_BASE + 'users',
  function(req, res) {
    const http_client = request_json.createClient(URL_mLab);
    console.log('Cliente HTTP mLab creado.');
    let field_params = 'f={"_id":0}&';
    http_client.get('user?' + field_params + apikey_mlab,
//    http_client.get('user?' + apikey_mlab,
      function(error, res_mlab, body) {
        console.log('Error: ' + error);
        console.log(`res_mlab: ${JSON.stringify(res_mlab)}`);
        console.log('Body: ' + body);
        let response = {};
        if(error) {
            response = {"msg" : "Error al recuperar users de mLab"}
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {"msg" : "Usuario no encontrado."};
            res.status(404);
          }
        }
        res.send(response);
      });
});

// Petición GET id con mLab
app.get(URL_BASE + 'users/:id',  //  /users/7
  function (req, res) {
    console.log('GET /api/2/users/:id');
    console.log(req.params.id);
    let id_user = req.params.id;  //  id = 7
    let queryString = 'q={"id_user":' + id_user + '}&';
    let fieldString = 'f={"_id":0}&';
    let httpClient = request_json.createClient(URL_mLab);
    httpClient.get('user?' + queryString + fieldString + apikey_mlab,
      function(error, res_mlab, body){
        console.log("Error: " + error);
        console.log(`res_mlab: ${JSON.stringify(res_mlab)}`);
        console.log("body" + body);
        var response = {};
        //console.log("resMLab: " + JSON.stringify(resMLab));
        if(error) {
            response = {"msg" : "Error en la peticion a mLab"};
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {"msg" : "Usuario no encontrado."}
            res.status(404);
          }
        }
        res.send(response);
      });
});

// GET cuentas del usuario con parametro ID - collecto user-accounts
app.get(URL_BASE + 'users/:id/accounts',
  function (req, res) {
    console.log("GET users/:id/accounts");
    console.log(req.params.id);
    let id = req.params.id;
    console.log(id);
    let queryString = 'q={"id_user":' + id + '}&';
    //let fieldString = 'f={"_id":0}&';
    let cuentas= 'f={"accounts":1, "_id":0}&';
    let httpClient = request_json.createClient(URL_mLab);
    httpClient.get('user?' + queryString + cuentas + apikey_mlab,
      function(error, res_mlab, body){
        console.log("Error: " + error);
        console.log("Respuesta Mlab" + res_mlab);
        console.log(`res_mlab: ${JSON.stringify(res_mlab)}`);
        console.log("body" + body);
        var response = {};
        if (error){
          response = {"msg" : "Error en la peticion a mLab"};
          res.status(500);
        } else {
          if (body.length > 0) {
            response = body;
          } else {
            response = {"msg" : "Usuario con ID no encontrado"};
            res.status(404);
          }
        }
        res.send(response);
      });
  });

  // GET cuentas del usuario con parametro ID - collection accounts
  app.get(URL_BASE + 'users/:id/accounts2',
    function (req, res) {
      console.log("GET users/:id/accounts2");
      console.log(req.params.id);
      let id = req.params.id;
      let queryString = 'q={"userID":' + id + '}&';
      //let queryString = `q={"userID": ${id} )&`;
      let fieldString = 'f={"_id":0}&';
      let httpClient = request_json.createClient(URL_mLab);
      httpClient.get('account?' + queryString + fieldString + apikey_mlab,
        function(error, res_mlab, body){
          console.log("Error: " + error);
          console.log("Respuesta Mlab" + res_mlab);
          console.log(`res_mlab: ${JSON.stringify(res_mlab)}`);
          console.log("body" + body);
          let response = {};
          if (error){
            response = {"msg" : "Error en la peticion a mLab"};
            res.status(500);
          } else {
              response = !error ? body : {'msg':'usuario no tiene cuentas'};
            }
          res.send(response);
        });
    });

//POST of user
app.post(URL_BASE + 'users',
 function(req, res) {
  console.log("POST users con MLab");
  let httpClient = request_json.createClient(URL_mLab);
  httpClient.get('user?'+ apikey_mlab,
  function(error, res_mlab, body) {
      let newID=body.length + 1;
      console.log("newID:" + newID);
      var newUser = {
        "id" : newID,
        "first_name" : req.body.first_name,
        "last_name" : req.body.last_name,
        "email" : req.body.email,
        "password" : req.body.password
      };
      httpClient.post("user?" + apikey_mlab, newUser ,
       function(error, res_mlab, body_post) {
         console.log(body_post);
        // console.log(body.first_name);
         res.send(body_post);
     });
  });
});

//PUT of user
app.put(URL_BASE + 'users/:id',
function(req, res) {
  let id = req.params.id;
  let queryString = 'q={"id_user":' + id + '}&';
  var httpClient = request_json.createClient(URL_mLab);
  httpClient.get('user?'+ queryString + apikey_mlab,
  function(error, res_MLab , body) {
    var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
    console.log(req.body);
    console.log(cambio);
     httpClient.put(URL_mLab + 'user?' + queryString + apikey_mlab, JSON.parse(cambio),
      function(error, res_post, body_post) {
        console.log("body-post:"+ body_post);
        res.send(body_post);
     });
 });
});

// Petición PUT con id de mLab (_id.$oid)
  app.put(URL_BASE + 'usersmLab/:id',
    function (req, res) {
      let id = req.params.id;
      let userBody = req.body;
      let queryString = 'q={"id_user":' + id + '}&';
      let httpClient = request_json.createClient(URL_mLab);
      httpClient.get('user?' + queryString + apikey_mlab,
        function(err, respMLab, body){
          let response = body[0];
          console.log(body);     //Actualizo campos del usuario
          let updatedUser = {
            "id" : req.body.id,
            "first_name" : req.body.first_name,
            "last_name" : req.body.last_name,
            "email" : req.body.email,
            "password" : req.body.password
          };//Otra forma optimizada (para muchas propiedades)
          // var updatedUser = {};
          // Object.keys(response).forEach(key => updatedUser[key] = response[key]);
          // Object.keys(userBody).forEach(key => updatedUser[key] = userBody[key]);

         // PUT a mLab
          httpClient.put('user/' + response._id.$oid + '?' + apikey_mlab, updatedUser,
            function(error, respuestaMLab, body_put){
              var response = {};
              if(error) {
                  response = {
                    "msg" : "Error actualizando usuario."
                  }
                  res.status(500);
              } else {
                if(body.length > 0) {
                  response = body_put;
                } else {
                  response = {
                    "msg" : "Usuario actualizado correctamente."
                  }
                  res.status(404);
                }
              }
              res.send(response);
            });
        });
  });

//DELETE user with id
app.delete(URL_BASE + "users/:id",
  function(req, res){
    console.log("entra al DELETE");
    console.log("request.params.id: " + req.params.id);
    var id = req.params.id;
    var queryStringID = 'q={"id":' + id + '}&';
    console.log(URL_mLab + 'user?' + queryStringID + apikey_mlab);
    var httpClient = request_json.createClient(URL_mLab);
    httpClient.get('user?' +  queryStringID + apikey_mlab,
      function(error, respuestaMLab, body){
        var respuesta = body[0];
        console.log("body delete:"+ JSON.stringify(respuesta));
        httpClient.delete(URL_mLab + "user/" + respuesta._id.$oid +'?'+ apikey_mlab,
          function(error, respuestaMLab,body){
            res.send(body);
        });
      });
});

//Method POST login
 app.post(URL_BASE + "login",
  function (req, res){
    console.log("Login con Mlab");
    let email = req.body.email;
    let pass = req.body.password;
    let queryString = 'q={"email":"' + email + '","password":"' + pass + '"}&';
    let limFilter = 'l=1&';
    let httpClient = request_json.createClient(URL_mLab);
    httpClient.get('user?'+ queryString + limFilter + apikey_mlab,
      function(error, resMlab, body) {
        //console.log("entro al body:" + body);
        //console.log(`resMlab: ${JSON.stringify(resMlab)}`);
        var response = body[0];
        //console.log(response);
        if(!error) {
          if (body.length == 1) { // Existe un usuario que cumple 'queryString'
            let login = '{"$set":{"logged":true}}';
            httpClient.put('user?q={"id_user": ' + body[0].id_user + '}&' + apikey_mlab, JSON.parse(login),
            //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
              function(errPut, resPut, bodyPut) {
                console.log("Login Correcto");
                console.log("id: " + body[0].id_user);
                console.log("name: " + body[0].first_name);
                console.log("email : " + body[0].email);
                //console.log("body" + body);
                res.send({'msg':'Login correcto', 'user':body[0].email, 'id':body[0].id_user, 'name':body[0].first_name});
                // If bodyPut.n == 1, put de mLab correcto
              });
          }
          else {
            res.status(404).send({"msg":"Usuario no válido."});
          }
        } else {
          res.status(500).send({"msg": "Error en petición a mLab."});
        }
    });
});

//Method POST logout
 app.post(URL_BASE + "logout",
  function(req, res) {
    console.log("Logout con Mlab");
    let email = req.body.email;
    let queryString = 'q={"email": "' + email + '","logged":true}&';
    console.log(queryString);
    let httpClient = request_json.createClient(URL_mLab);
    httpClient.get('user?'+ queryString + apikey_mlab,
      function(error, resMlab, body) {
        var respuesta = body[0]; // Asegurar único usuario
        if(!error) {
          if (respuesta != undefined) { // Existe un usuario que cumple 'queryString'
            let logout = '{"$unset":{"logged":true}}';
            httpClient.put('user?q={"id_user": ' + respuesta.id_user + '}&' + apikey_mlab, JSON.parse(logout),
            //clienteMlab.put('user/' + respuesta._id.$oid + '?' + apikeyMLab, JSON.parse(logout),
              function(errPut, resPut, bodyPut) {
                res.send({'msg':'Logout correcto', 'user':respuesta.email});
                // If bodyPut.n == 1, put de mLab correcto
              });
            } else {
                res.status(404).send({"msg":"Logout failed!"});
            }
        } else {
          res.status(500).send({"msg": "Error en petición a mLab."});
        }
    });
});

//Devolver numero total de usuarios
 app.get(URL_BASE + 'total_users',
   function(request, response){
     let tam = usersFile.length;
     console.log('numero total de usuarios: ' + tam);
     response.status(200).send({"num_usuarios " : tam});
 });
